// const person = {
//   name: "Maxim",
//   age: 25,
//   greet: function () {
//     console.log("Greet!");
//   },
// };

const person = new Object({
  name: "Maxim",
  age: 25,
  greet: function () {
    console.log("Greet!");
  },
});

const person2 = person;

Object.prototype.sayHello = function () {
  console.log("Hello!!!");
};

const lena = Object.create(person);
lena.name = "Elena";

class Samurai {
  constructor(name) {
    this.name = name;
  }

  hello() {
    alert(this.name);
  }
}

let shogun = new Samurai("Oleg");
console.log(shogun.__proto__.__proto__ === Object.prototype);
console.log(shogun.__proto__.constructor.__proto__ === Function.prototype);
console.log(shogun.__proto__.__proto__.__proto__ === null);
