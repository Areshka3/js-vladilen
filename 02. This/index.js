function sayHello() {
  console.log(this);
}

const person = {
  name: "Vasya",
  age: 30,
  sayHello,
  sayHelloWindow: sayHello.bind(window),
  logInfo: function (job, phone) {
    console.group(`${this.name} info:`);
    console.log(`Name is ${this.name}`);
    console.log(`Job: ${job || "unemployed"}`);
    console.log(`Phones: ${phone || "-"}`);
    console.groupEnd();
  },
};

const lena = {
  name: "Elena",
  age: 23,
};

person.logInfo.bind(lena, "FrontEnd", "099999999")();
person.logInfo.call(lena, "FrontEnd", "099999999");
person.logInfo.call(lena, ...["FrontEnd", "099999999"]);
person.logInfo.apply(lena, ["FrontEnd", "099999999"]);

// =======================================

const array = [1, 2, 3, 4, 5];

Array.prototype.multBy = function(n) {  
  return this.map((el) => el * n);
};

console.log(array.multBy(2))