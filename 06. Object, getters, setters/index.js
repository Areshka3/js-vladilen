const person = Object.create(
  {
    calculateAge() {
      console.log("Age:", new Date().getFullYear() - this.birthYear);
    },
  },
  {
    name: {
      value: "Oleg",
      enumerable: true, // 1) указывает, что свойство является перечисляемым
      writable: true, // 2) указывает, что свойство можено изменить
      configurable: true, // 3) указывает, что можно удалять ключ
    },
    birthYear: {
      value: 1990,
    },
    age: {
      enumerable: true,
      get() {
        return new Date().getFullYear() - this.birthYear;
      },
      set(value) {
        console.log("Set age:", value);
      },
    },
  }
);

// 2)
// person.name = "Maxim";
// person.birthYear = 2000;

console.log(person);

// 1)
console.group("first");
for (const key in person) {
  console.log("key:", key, "value:", person[key]);
}
console.groupEnd();

console.group("second");
for (const key in person) {
  if (person.hasOwnProperty(key)) {
    console.log("key:", key, "value:", person[key]);
  }
}
console.groupEnd();

// 3)
// delete person.name; // удалиться
// delete person.birthYear; // не удалиться
// console.log(person);
