console.log("Request data...");

// setTimeout(() => {
//   console.log("Preparing data...");

//   const backendData = {
//     server: "aws",
//     port: 2000,
//     status: "working",
//   };

//   setTimeout(() => {
//     backendData.modified = true;
//     console.log("Data received...", backendData);
//   }, 2000);
// }, 2000);

// ================================================================

// const promise = new Promise((resolve, rej) => {
//   setTimeout(() => {
//     console.log("Preparing data...");

//     const backendData = {
//       server: "aws",
//       port: 2000,
//       status: "working",
//     };

//     resolve(backendData);
//   }, 2000);
// });

// promise
//   .then((data) => {
//     return new Promise((resolve, reject) => {
//       setTimeout(() => {
//         data.modified = true;
//         resolve(data);
//         // reject("aaaaaaaaaaaaaaaa");
//       }, 2000);
//     });
//   })
//   .then((clientData) => {
//     console.log("Data received...", clientData);
//   })
//   .catch((err) => console.error("Error: " + err))
//   .finaly(() => console.log("Finaly"));

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  });
};

// sleep(2000).then(() => console.log("After 2 sec"));
// sleep(3000).then(() => console.log("After 3 sec"));

Promise.all([sleep(2000), sleep(4000)]).then(() => {
  console.log("All promises");
});
// сработает когда выполняться все переданные промисы
// Prmise.all нужен, например, чтобы получить данные с разных ендпоинтов и скомбинировать эти данные

Promise.race([sleep(2000), sleep(4000)]).then(() => {
  console.log("Race promises");
});
// сработает когда выполнится хотябы один из промисов
