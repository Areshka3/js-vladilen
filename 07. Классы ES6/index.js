class Animal {
  static id = 1;

  constructor(options) {
    this.id = Animal.id++;
    this.name = options.name || "no name";
    this.age = options.age || 0;
    this.hasTail = options.hasTail;
  }

  voice() {
    console.log("I am Animal");
  }
}

class Cat extends Animal {
  static type = "CAT";

  constructor(options) {
    super(options);
    this.type = Cat.type;
    this.color = options.color;
  }

  voice() {
    super.voice();
    console.log("I am cat");
  }

  get ageInfo() {
    return this.age * 7;
  }

  set ageInfo(newAge) {
    this.age = newAge;
  }
}

const cat = new Cat({ hasTail: true, name: "Vasya", age: 5, color: "black" });

class Dog extends Animal {
  static type = "DOG";

  constructor(options) {
    super(options);
    this.type = Dog.type;
  }

  voice() {
    super.voice();
    console.log("I am dog");
  }
}

const dog = new Dog({ hasTail: true, name: "Polkan" });

// =====================================================================
const block1 = document.createElement("div");
block1.setAttribute("id", "box1");
document.body.appendChild(block1);

class Component {
  constructor(selector) {
    this.$el = document.querySelector(selector);
  }

  hide() {
    this.$el.style.display = "none";
  }
  show() {
    this.$el.style.display = "block";
  }
}

class Box extends Component {
  constructor(options) {
    super(options.selector);
    this.$el.style.width = this.$el.style.height = options.size + "px";
    this.$el.style.background = options.color;
  }
}

const box1 = new Box({
  selector: "#box1",
  size: 100,
  color: "red",
});

box1.hide();
box1.show();

// ==========================================================================
const block = document.createElement("div");
block.setAttribute("id", "box1");
document.body.appendChild(block);

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    let output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    block.innerText = output;
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

let clock = new Clock({ template: "h:m:s" });
clock.start();
