// function createCalcFun(n) {
//   return function () {
//     console.log(1000 * n);
//   };
// }

// const calc = createCalcFun(42);
// calc();

// =============================================================

function createIncrentor(n) {
  return function (num) {
    return n + num;
  };
}

const addOne = createIncrentor(1);
console.log(addOne(10));
console.log(addOne(20));
const addTen = createIncrentor(10);
console.log(addTen(10));
console.log(addTen(20));

// ==========================================================

function urlGenerator(domain) {
  return function (url) {
    return `https://${url}.${domain}`;
  };
}

const comUrl = urlGenerator("com");
console.log(comUrl("google"));
console.log(comUrl("test"));

const ruUrl = urlGenerator("ru");
console.log(ruUrl("google"));
console.log(ruUrl("test"));

// ===============================================================
function logPerson() {
  console.log(`Person: ${this.name}, ${this.age}`);
}

const person1 = { name: "Vasya", age: 25 };
const person2 = { name: "Petya", age: 30 };

function customBind(context, fn) {
  return function (...args) {
    fn.apply(context, args);
  };
}

const person1Info = customBind(person1, logPerson);
person1Info();
const person2Info = customBind(person2, logPerson);
person2Info()
